#ifndef __GLIB_CONTROLLER_H__
#define __GLIB_CONTROLLER_H__

#define __GLIB_CONTROLLER_H_INSIDE__

#include "gcontrollertypes.h"
#include "gcontrollerevent.h"
#include "gcontroller.h"

#include "giterable.h"
#include "giterator.h"

#include "garraycontroller.h"
#include "ghashcontroller.h"
#include "gptrarraycontroller.h"

#include "gcontrollerversion.h"
#include "gcontrollerenumtypes.h"

#undef __GLIB_CONTROLLER_H_INSIDE__

#endif /* __GLIB_CONTROLLER_H__ */
